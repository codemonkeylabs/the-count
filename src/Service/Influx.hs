module Service.Influx where

import Control.Concurrent             (threadDelay)
import Control.Monad.Trans.Writer     (WriterT(..), execWriterT, tell)
import Data.Array.IO                  (IOUArray)
import Data.Array.MArray              (MArray, readArray)
import Data.ByteString.Builder        (Builder, lazyByteString, toLazyByteString)
import Data.Count
import Data.Function                  (on)
import Data.Hashable                  (Hashable)
import Data.IP                        (IP(..))
import Network.Socket
import Network.Socket.ByteString.Lazy (sendAll)
import System.Clock                   (Clock(Monotonic), TimeSpec(..), getTime)


import qualified Data.ByteString.Lazy.Char8 as LB
import qualified Data.List                  as L
import qualified Data.IntMap.Strict         as Map

-- For consistency on the names of the type variables used accross this module
-- i is for the type of the index used in counter fields
-- e is for the type of the element used in counter fields
-- j is for the type of the index used in tags fields
-- t is for the type of the element used in tags fields

data Context i e j t = Context { getName     :: LB.ByteString,
                                 getConfig   :: Configuration,
                                 getCounts   :: CList i e j t,
                                 getSocket   :: Maybe Socket }

data Configuration = Configuration { getHostIP   :: IP,
                                     getHostPort :: PortNumber,
                                     getWindow   :: Int,
                                     getMaxLen   :: Int64,
                                     warning     :: LogFn,
                                     info        :: LogFn,
                                     debug       :: LogFn }

type LogFn = String -> IO ()

instance Default Configuration where
    def = Configuration {
            getHostIP   = "127.0.0.1",
            getHostPort = 8089,
            getWindow   = 5000000, -- a bit of a default SWAG
            getMaxLen   = 512, -- 512 is the safest max UDP payload
                               -- size. On local environments it can
                               -- be increased for better performance
                               -- to something closer to interface
                               -- MTU.
            warning     = putStrLn,
            info        = putStrLn,
            debug       = const $ return ()
          }

data InfluxException = BadConfig String
                     | NoSuchHost String
                     | NotReady String
                       deriving (Show, Typeable)

instance Exception InfluxException

type CList i e j t = IORef [Count i e j t]

class (Eq a, Show a) => Key a where
  showKey :: a -> LB.ByteString

class (Eq a) => FieldValue a where
  showField :: a -> LB.ByteString

instance FieldValue LB.ByteString where
  showField str = "\"" <> escapeField str <> "\""
    where
      escapeField s
        | '"' `LB.elem` s = LB.concatMap esc s
        | otherwise       = s
        where esc '"' = "\\\""
              esc c   = LB.singleton c

instance FieldValue Double where
  showField = LB.pack . show

instance FieldValue Int where
  showField = showField @Int64 . fromIntegral

instance FieldValue Int64 where
  showField i = LB.pack (show i <> "i")

instance FieldValue Bool where
  showField True  = "true"
  showField False = "false"

escapeTag :: LB.ByteString -> LB.ByteString
escapeTag s
  | any (`LB.elem` s) ['=',',',' '] = LB.concatMap escape s
  | otherwise = s
  where escape '=' = "\\="
        escape ',' = "\\,"
        escape ' ' = "\\ "
        escape c   = LB.singleton c

type DataLine i e j t = (Key i, FieldValue e, Key j, Enum j, Num e, Hashable t, t ~ LB.ByteString)

createService :: (FieldIndex i, DataLine i e j t, MonadIO m) =>
                 LB.ByteString ->
                 Configuration ->
                 m (Context i e j t)
createService name config = do
  counts <- liftIO $ newIORef mempty
  return Context { getName     = name,
                   getSocket   = Nothing,
                   getConfig   = config,
                   getCounts   = counts
                 }

runService :: (FieldIndex i, DataLine i e j t, MonadIO m,
               MArray IOUArray e IO) => Context i e j t -> m ()
runService context@Context{getConfig=Configuration{..}, ..} = liftIO . withSocketsDo $
  bracket connectTo disconnect $ \sk -> do
      let ?context = context { getSocket = Just sk }
      info $ "Starting Influx service to " ++ host ++ " for " ++ LB.unpack getName
      serviceLoop
      where connectTo = do
                result <- runMaybeT $ do
                  hostAddr <- MaybeT $ resolve getHostIP getHostPort
                  liftIO $ do
                    sk <- socket (addressFamily getHostIP) Datagram 0
                    connect sk hostAddr
                    return sk
                maybe badNetworkAddress return result
            disconnect sk = do
              info $ "Stopping " ++ LB.unpack getName ++ " service to " ++ host
              close sk
            badNetworkAddress = throw $ NoSuchHost host
            host = show getHostIP ++ ':':show getHostPort

serviceLoop :: (?context::Context i e j t,
                FieldIndex i, DataLine i e j t,
                MArray IOUArray e IO) => IO ()
serviceLoop = do
  let Context{getConfig=Configuration{..}, ..} = ?context
  startTime <- timeSinceStart
  counters  <- atomicModifyIORef' getCounts (mempty, )
  mapM_ export counters
  now       <- timeSinceStart
  let elapsed  = fromIntegral $ (now - startTime) `div` 1000
      sleepFor = getWindow - elapsed
  if sleepFor > 0 then do
      debug $ "Finished all scheduled activity. Will run in " ++ show sleepFor ++ "µS"
      threadDelay sleepFor
  else
      warning $ "Statistics exporting for " ++ LB.unpack getName
                   ++ " might be falling behind monitored activity. Consider increasing"
                   ++ " current export window of " ++ show getWindow ++ "uS"
  serviceLoop

dispatch :: MonadIO m => Count i e j t -> Context i e j t -> m ()
dispatch counter Context{..} =
  liftIO $ atomicModifyIORef' getCounts $ (, ()) . (counter:)

export :: (?context::Context i e j t,
           FieldIndex i, DataLine i e j t,
           MArray IOUArray e IO)
          => Count i e j t
          -> IO ()
export Count{..} = do
  startTime <- timeSinceStart
  debug (getConfig ?context) $ "Exporting stats @" ++ show startTime
  Measurements measurements <- atomicModifyIORef' getMeasurements (Measurements Map.empty, )
  (elems, _, bs)  <- foldM serializeMeasurements (0, 0, []) measurements
  now <- timeSinceStart
  debug (getConfig ?context) $ "Serialized " ++ show elems ++ " data points in " ++ show ((now - startTime) `div` 1000) ++ "µS"
  mapM_ (send.toLazyByteString) bs
    where send b = do
            debug (getConfig ?context) $ "Sending " ++ show (LB.length b) ++ " byte datagram to " ++ host
            sendAll (fromJust $ getSocket ?context) b
          host = show (getHostIP . getConfig $ ?context) ++ ':':show (getHostPort . getConfig $ ?context)
          serializeMeasurements = foldM serializeMeasurement

serializeMeasurement :: (?context::Context i e j t,
                        FieldIndex i, DataLine i e j t, MArray IOUArray e IO)
                        => (Int, Int64, [Builder])
                        -> Measurement i e t
                        -> IO (Int, Int64, [Builder])
serializeMeasurement acc@(elems, currentLen, builders) (Measurement tags fields loaded) =
  readIORef loaded >>= \case
    True -> do
      line <- serialize (toTagsList tags) fields
      let lineLength = LB.length line
          newLength  = currentLen + lineLength
          newBuilder = lazyByteString line
          elems'     = elems + 1
      return $ if newLength > (getMaxLen . getConfig $ ?context) || null builders then
                   (elems', lineLength, newBuilder:builders)
               else let (builder:rest) = builders
                    in (elems', newLength, mappend newBuilder builder : rest)
    False ->
      return acc

serialize :: (?context::Context i e j t,
              FieldIndex i, DataLine i e j t, MArray a e IO)
             => [Field j t]
             -> a i e
             -> IO LB.ByteString
serialize tags fields = do
  let Context{getConfig=Configuration{..}} = ?context
  debug $ "Serializing " ++ show tags
  execWriterT $ do
    tell $ getName ?context
    serializeTags tags
    tell " "
    serializeFields fields
    tell "\n"

serializeTags :: (Key i) => [Tag i LB.ByteString] -> WriterT LB.ByteString IO ()
serializeTags tags =
  mapM_ format $ groupedTags tags
    where groupedTags     = L.groupBy ((==) `on` fst)
          format []       = return ()
          format [(k, v)] = tell $ "," <> showKey k <> "=" <> escapeTag v -- ",foo=x"
          format ts@((k,_):_) = do -- ",foo=\"x,y\""
            let vals = LB.intercalate "\\," $ map (escapeTag . snd) ts
            tell $ "," <> showKey k <> "=" <> vals

serializeFields :: (FieldIndex i, Key i, FieldValue e, Num e, MArray a e IO)
                   => a i e
                   -> WriterT LB.ByteString IO ()
serializeFields fields = do
  bs <- lift $ foldM format [] [minBound..maxBound]
  mapM_ tell $ L.intersperse "," bs -- "foo=x,bar=y"
    where format acc i = do
            x <- readArray fields i
            return $ if x == 0
              then acc
              else showKey i <> "=" <> showField x : acc

resolve :: IP -> PortNumber -> IO (Maybe SockAddr)
resolve ip@IPv4{} port = resolve' AF_INET ip port
resolve ip@IPv6{} port = resolve' AF_INET6 ip port

resolve' :: Family -> IP -> PortNumber -> IO (Maybe SockAddr)
resolve' family ip port = do
  let hint = Just (defaultHints { addrFamily = family })
  infos <- getAddrInfo hint (Just $ show ip) (Just $ show port)
            `catch` \(_::SomeException) ->
                return []
  return $ addrAddress <$> listToMaybe infos

addressFamily :: IP -> Family
addressFamily IPv4{} = AF_INET
addressFamily IPv6{} = AF_INET6

timeSinceStart :: MonadIO m => m Int64
timeSinceStart = liftIO $
    getTime Monotonic >>= \(TimeSpec secs ns) ->
        return $ secs * 1000000000 + ns
