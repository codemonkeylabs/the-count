{- |
Description: C.O.U.N.T - Counter Organization for Unified Network TimeSeries
Copyright: (c) Secucloud GmbH, 2017-2019
License: LGPL
Maintainer: erick@codemonkeylabs.de

-}
module Data.Count ( Count(..)
                  , Field
                  , Tag
                  , FieldIndex
                  , Measurement(..)
                  , Measurements(..)
                  , (.=.)
                  , count
                  , fromTagsList
                  , label
                  , newCount
                  , newMeasurement
                  , newMeasurementFrom
                  , post
                  , tabulate
                  , toTagsList ) where
import Data.Array.IO                  (IOUArray)
import Data.Array.MArray              (MArray, Ix, newArray, readArray, writeArray)
import Data.Bifunctor                 (first)
import Data.IntMap.Strict             (IntMap)

import qualified Data.ByteString.Lazy.Char8  as LB
import qualified Data.IntMap.Strict          as Map

newtype VonCountException = IllegalOperation String
                            deriving (Show, Typeable)

instance Exception VonCountException

-- For consistency on the names of the type variables used accross this module
-- i is for the type of the index used in counter fields
-- e is for the type of the element used in counter fields
-- j is for the type of the index used in tags fields
-- t is for the type of the element used in tags fields

newtype Measurements i e j t = Measurements (IntMap [Measurement i e t])

data Measurement i e t = Measurement {
                           tags     :: IntMap t
                         , counters :: IOUArray i e
                         , loaded   :: IORef Bool
                       }

newtype Count i e j t = Count { getMeasurements :: IORef (Measurements i e j t)}

class (Ord a, Ix a, Bounded a, Enum a)  => FieldIndex a

type Field a b  = (a, b)
type Tag a b  = (a, b)

infix 2 .=.

(.=.) :: (Show b) => a -> b -> Tag a LB.ByteString
(.=.) a b = (a, LB.pack $ show b)

toTagsList :: Enum j => IntMap t -> [Tag j t]
toTagsList = fmap (first toEnum) . Map.toList

fromTagsList :: Enum j => [Tag j t] -> IntMap t
fromTagsList = Map.fromList . fmap (first fromEnum)

newCount :: MonadIO m => m (Count i e j t)
newCount = Count <$> newIORef (Measurements Map.empty)

-- Achtung: This is not thread safe! for the sake of perf. You need to call this
-- ensuring exclusive access from one thread to the Count
count :: (MonadIO m, FieldIndex i, Enum j, Ord j, Ord t, Num e, MArray IOUArray e IO)
      => Count i e j t
      -> Int
      -> [Field i e]
      -> [Tag j t]
      -> m ()
count Count{..} ix fields tags = do
  measurements  <- readIORef getMeasurements
  measurements' <- count' measurements ix fields tags
  atomicWriteIORef getMeasurements measurements'

count' :: (FieldIndex i, Enum j, Ord j, Ord t, Num e, MArray IOUArray e IO, MonadIO m)
       => Measurements i e j t
       -> Int
       -> [Field i e]
       -> [Tag j t]
       -> m (Measurements i e j t)
count' (Measurements measurements) ix fields tags = do
  let entry = Map.lookup ix measurements
  entry' <- maybe mkMeasurement update entry
  return $ Measurements (Map.insert ix entry' measurements)
    where update (entry:rest) = do
            tabulate fields entry
            return (label tags entry : rest)
          update []     = mkMeasurement
          mkMeasurement = do
            measurement <- newMeasurement tags
            tabulate fields measurement
            return [measurement]

newMeasurement :: (MArray IOUArray e IO, FieldIndex i, Num e, Enum j, MonadIO m)
               => [Tag j t] -> m (Measurement i e t)
newMeasurement tags =
  Measurement (fromTagsList tags) <$> liftIO (newArray (minBound, maxBound) 0) <*> newIORef False

newMeasurementFrom :: (MArray IOUArray e IO, FieldIndex i, Num e, MonadIO m)
                   => Measurement i e t -> m (Measurement i e t)
newMeasurementFrom (Measurement tags _ _)  =
  Measurement tags <$> liftIO (newArray (minBound, maxBound) 0) <*> newIORef False

tabulate :: (Ix i, MArray IOUArray e IO, MonadIO m, Num e) => [Field i e]
                                                           -> Measurement i e t
                                                           -> m ()
tabulate fields (Measurement _ a loaded) = do
  forM_ fields $ \(k, v) -> liftIO $ do
     x <- readArray a k
     writeArray a k $ x + v
  atomicModifyIORef' loaded $ const (True, ())

label :: Enum j => [Tag j t] -> Measurement i e t -> Measurement i e t
label tags (Measurement tags0 a loaded) =
  Measurement (fromTagsList tags `Map.union` tags0) a loaded

post :: (MArray IOUArray e IO, FieldIndex i, Num e, MonadIO m) => Measurement i e t
                                                               -> Int
                                                               -> Count i e j t
                                                               -> m ()
post measurement@(Measurement _ _ loaded) ix Count{..} = do
  readIORef loaded >>= \case
    True ->
      atomicModifyIORef' getMeasurements $ \(Measurements measurements) ->
        (Measurements $ Map.alter post' ix measurements, ())
    False ->
      return ()
  where post' Nothing     = return [measurement]
        post' (Just prev) = return (measurement:prev)
