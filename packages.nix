#
{dev, ...}: self: super: let

in {
  mkDerivation = args : super.mkDerivation (args // {
    doCheck   = false;
    doHaddock = false;
    enableLibraryProfiling = dev;
  });

  ghc = super.ghc;

  the-count = self.callCabal2nix "the-count" (
               builtins.filterSource (path: type: type != "directory" ||
                                      baseNameOf path != ".git") ./. ) { };

  not-prelude = self.callCabal2nix "not-prelude" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/not-prelude";
      ref    = "master";
    }) {};
  # overriden nixpkgs packages we need...

}
